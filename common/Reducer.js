const initialState={
    data:[],
    isLoading:false,
    page:0,
    isLogin:false
}
export default (state=initialState,action)=>{
   
    switch(action.type){
        case "login":
            return {...state,
                isLogin:true
            }
        case "fetch":
            return {...state,
                    isLoading:true
            };
        case "addFetch":
            return{...state,
                    data:[...state.data,...action.dataApi]
            };
        case "Page":
            return{...state,
                data:[],
                page:0
            }
        default:return{...state};
    }
    return state;
}