import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {profil} from "./common/User";
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Provider } from "react-redux";
import Detail from './Component/HalamanDetail'
import AboutUs from './Component/AboutUs'
import { createStore } from "redux";
import Reducer from "./common/Reducer";
import HomeScreen from './Component/HomeScreen';
import LoginScreen from './Component/LoginScreen';
const store = createStore(Reducer)
const LoginStack = createStackNavigator();
const BodyStack = createStackNavigator();
const DrawerProfilStack = createDrawerNavigator();
const BottomStackNav = createBottomTabNavigator()
const DrawerProfilScreen = () => (
  <DrawerProfilStack.Navigator>
    {profil.Profils.map((val,index)=>(
      
      <DrawerProfilStack.Screen key={index} name={val.name} component={AboutUs} initialParams={val}></DrawerProfilStack.Screen>
    ))}
  </DrawerProfilStack.Navigator>
)
const BodyStackScreen = () => (
  <BodyStack.Navigator>
    <BodyStack.Screen name="Menu" component={HomeScreen}></BodyStack.Screen>
    <BodyStack.Screen name="Detail" component={Detail}></BodyStack.Screen>
  </BodyStack.Navigator>
)
const BottomNav = () => (
  <BottomStackNav.Navigator>
    <BottomStackNav.Screen name="Menu" options={{
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="home" size={25} />
      )
    }} component={BodyStackScreen} />
    <BottomStackNav.Screen name="Profil" options={
      {
        tabBarLabel: 'Profil',
        tabBarIcon: () => (
          <Icon name="supervisor-account" size={25} />
        ),
      }
    } component={DrawerProfilScreen} />
  </BottomStackNav.Navigator>
)
const RootStackScreen = () => (
  <LoginStack.Navigator headerMode={false}>
    <LoginStack.Screen name="Login" component={LoginScreen}></LoginStack.Screen>
    <LoginStack.Screen name="Menu" component={BottomNav}></LoginStack.Screen>
  </LoginStack.Navigator>
)
export default class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <RootStackScreen>

          </RootStackScreen>
        </NavigationContainer> 
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
