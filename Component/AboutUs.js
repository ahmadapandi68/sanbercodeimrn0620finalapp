import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fa from 'react-native-vector-icons/AntDesign';
export default class App extends Component {
	render(){
	  return(
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"white"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('../images/logo.png')} style={{ width: 360, height: 55 }} />
	  	</View>
	  	<View style={styles.cheader}><Text style={{fontWeight:'bold'}}>Tentang Saya</Text></View>
	  	<View style={styles.content}>
	  		<Icon style={styles.people} name="account-circle" size={100} />
	  		<Text style={styles.identity}>Ahmad Apandi</Text>
        <Text style={styles.identity3}>Copaser</Text>
        <View style={styles.box}>
          <Text style={styles.identity2}>Fortofolio</Text>
          <Text style={{borderWidth:1,width:340,borderColor:'black',marginTop:10,alignSelf:'center'}}></Text>
          <View style={styles.page}>
            <View style={styles.satu}>
              <Fa name="gitlab" style={{padding:5,textAlign:'center',color:"#3EC6FF"}} size={50} />
              <Text style={styles.identity2}>@dreamsCibatok</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="github" style={{padding:5,textAlign:'center' ,color:"#3EC6FF"}} size={50} />
              <Text style={styles.identity2}>@dreamsCibatok</Text>
            </View>
          </View>
        </View>
        <View style={styles.box}>
          <Text style={styles.identity2}>Tentang Saya</Text>
          <Text style={{borderWidth:1,width:340,borderColor:'black',marginTop:10,alignSelf:'center'}}></Text>
          <View style={styles.page2}>
            <View style={styles.pageOnPage}>
              <Fa name="facebook-square" style={{padding:5,textAlign:'center' ,color:"#3EC6FF"}} size={50} />
              <Text style={styles.identity2}>@dreamsCibatok</Text>
            </View>
            <View style={styles.pageOnPage}>
              <Fa name="twitter" style={{padding:5,textAlign:'center',color:"#3EC6FF"}} size={50} />
              <Text style={styles.identity2}>@dreamsCibatok</Text>
            </View>
          </View>
        </View>
	  	</View>
	  </View>
	  )
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#84c968'
  },
  header: {
  	marginTop:50,
  	height: 120,
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#EFEFEF',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:400,
  },
  content:{
    
  	height:600,
  	padding:20
  },
  box:{
    marginTop:20,
    backgroundColor:'#EFEFEF',
    height:180,
    width:360,
    borderRadius:10,

  },
  people:{
    textAlign:'center',
    height:55,
    width:360
  },
  identity:{
    textAlign:'center',
    marginTop:60,
    color:'grey',
    fontFamily:'Roboto',
    color:'#036'
  },
  identity2:{
    textAlign:'left',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    fontFamily:'Roboto',
    color:'#036'
  },
  identity3:{
    textAlign:'center',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    fontFamily:'Roboto',
    color:'#036'
  },
  page:{
    width:360,
    height:150,
    padding:10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  pageOnPage:{
    width:360,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  page2:{
    width:360,
    height:150,
    padding:10,
    alignItems: 'center',
  },
  satu:{
    width:170,
    height:150,
    padding:20,
    textAlign:'center'
  }
});
