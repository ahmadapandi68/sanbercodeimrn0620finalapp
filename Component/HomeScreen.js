import React,{Component} from 'react'
import {View,StyleSheet,Text,Image,TouchableOpacity, TextInput, FlatList, findNodeHandle,
    ActivityIndicator
} from 'react-native'
import {connect} from "react-redux";
import axios from "axios";
class ProvinsiItem extends Component{
    render(){
        console.log(this.props);
        return(
            <View style={style.ProvinsiItem}>
                        <View style={style.provinsiDesc}>
        <Text style={style.provinsiTitle}>Provinsi {this.props.provinsi.item.attributes.Provinsi}</Text>
                            <View style={style.rating}>
                                <Image source={require("../images/sad.png")} style={style.imageLogo}></Image>
                                <Text style={{marginLeft:10}} >Positif {this.props.provinsi.item.attributes.Kasus_Posi}</Text>
                            </View>
                            <View style={style.rating}>
                             <Image source={require("../images/happy.png")} style={style.imageLogo} ></Image>
                                <Text style={{marginLeft:10}}>Sembuh  {this.props.provinsi.item.attributes.Kasus_Semb}</Text>
                            </View>
                            <View style={style.rating}>
                            <Image source={require("../images/cry.png")} style={style.imageLogo}></Image>
                             <Text style={{marginLeft:10}}>Meninggal  {this.props.provinsi.item.attributes.Kasus_Meni}</Text>
                            </View>
                            <TouchableOpacity style={style.provinsiButton} onPress={this.props.goToDetail}>
                                <Text style={{color:'white'}}>Detail </Text>
                            </TouchableOpacity>
                        </View>
                </View>
        )
    }
}
class HomeScreen extends Component{
    constructor(props){
        super(props)
        this.state={
            url:`http://sioman.nufaza.co.id:8000/apikoronaprovinsi.php`
        
        }
    }
    fetchDataApi(){
            this.props.fetching();
            axios.get(`${this.state.url}`).then(result=>{
                const dataArr = result.data;
                this.props.addData(dataArr);
            }).catch(err=>{
                console.log(err);
                console.log("error");
            })
        
    }
    componentDidMount(){
        this.fetchDataApi();
    }
    refresh(){
        this.props.refreshing();
        this.fetchDataApi(this.props.page)
    }
    multipleFlatList(){
        let array=[];
        for(let i=0;i<this.props.data.length;i++){
            let flatlist=(<ProvinsiItem provinsi={{item:this.props.data[i]}}/>)
            array.push(flatlist)
        }
        return array;
    }
    goToDetail(provinsi){
        this.props.navigation.push("Detail",{item:provinsi})
    }
    render(){
        return(
            <View style={style.container}>
                <View style={style.provinsiList}>
                <Text style={{marginVertical:10,marginLeft:40, fontFamily:"Roboto",fontSize:20}}>{this.state.isSearchedMode?`Menampilkan Provinsi "${this.state.query}"`:"Daftar Provinsi"}</Text>
                
                   <FlatList 
                   style={{marginLeft:40}}
                    
                    data={this.props.data}
                    renderItem={(negara)=>{
                        return <ProvinsiItem provinsi={negara} goToDetail={()=>this.goToDetail(negara)}/>
                    }}
                    />
                </View>

            </View>
        )
    }
}
const style=StyleSheet.create({
    imageLogo: {
        height: 10,
        width: 10
    },
    provinsiButton:{
        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        padding: 5,

        elevation: 24,
        borderRadius: 20,
        alignItems: 'center',
        width: '100%',
        marginHorizontal: 40,
        marginVertical: 20,
        backgroundColor: '#34772d'
    },
    rating:{
        flexDirection:"row",
        marginTop:10,
    },
    provinsiGenre:{
        fontFamily:"Roboto",
        fontSize:15,
        fontWeight:'100',
        marginTop:5,
    },
    provinsiTitle:{
        fontSize:18,
        fontWeight:'bold',

    },
    provinsiDesc:{
        paddingTop:10,
        width:'50%',
        marginLeft:20,
    },
    provinsiImage:{
        width:200,
        height:200,
        resizeMode:'contain',
        borderRadius:10,
    },
    ProvinsiItem:{
        flexDirection:'row',
        marginVertical:10,
        backgroundColor:'#84c968',
        width:'80%'
    },
    provinsiList:{
        flex:1,
        marginTop:20,
        marginRight:5
    },
    searchHeader:{
        marginTop:40,
        marginLeft:40,
        flexDirection:'row',
        height:50,
        width:'80%',
        borderTopStartRadius:20,
        borderBottomStartRadius:20,
        backgroundColor:'#DCD3D3'
    },
    container:{
        
        flex:1,
        backgroundColor: '#deffd2'
    }
})
const stateProp=(state)=>{
    return{
        data:state.data,
        page:state.page,
        isLoading:state.isLoading
    }
}
const actionProp=()=>{
    return {
        fetching:() =>{return {type:"fetch"}},
        addData:(data)=>{return{type:'addFetch',dataApi:data}},
        refreshing:()=>{return{type:'refreshed'}},
        returnPage:()=>{return{type:'Page'}}
    }
}
export default connect(stateProp,actionProp())(HomeScreen)