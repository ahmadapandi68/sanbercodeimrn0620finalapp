import React, { Component, isValidElement } from 'react';

import {connect} from "react-redux";
import {profil} from "../common/User";
import {
    StyleSheet, Text, View
    , Image, TextInput, TouchableOpacity
} from 'react-native';

class LoginScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            email:"",
            password:"",
            failedLogin:false,
            pesanError:""
        }
    }
    login(){
        const emailCheck=profil.Profils.filter((val)=>val.email==this.state.email);
        if(emailCheck.length==0){
            this.setState({
                failedLogin:true,
                pesanError:"Email anda Salah"
            });
        }else{
            const passwordCheck=emailCheck.filter((val)=>val.password==this.state.password);
            if(passwordCheck.length==0){
                this.setState({
                    failedLogin:true,
                    pesanError:"Password anda Salah"
                });
            }else{
                this.props.toLogin();
            }
        }
    }
    render() {
        if(this.props.isLogin)
            this.props.navigation.push("Menu")
        return (
            <View style={style.container}>
                <View style={style.logoHeader}>
                    <Image style={style.imageLogo} source={require("../images/logo.png")}></Image>
                    <Text style={{ fontSize: 40, fontFamily: "notoserif" }}>Kawal Korona</Text>
                </View>
                <View style={{ margin: 40 }}>
                    <Text style={{ fontSize: 25, fontFamily: "notoserif"  }}>Login</Text>
                </View>
                <View style={style.inputStyle}>
                    <TextInput placeholderTextColor='black' placeholder="Email " 
                        onChangeText={(text)=>{
                            this.setState({
                                email:text
                            })
                        }}
                        value={this.state.email}
                    ></TextInput>
                </View>
                <View style={style.inputStyle}>
                    <TextInput placeholderTextColor='black' secureTextEntry={true} placeholder="Password "
                    onChangeText={(text)=>{
                        this.setState({
                            password:text
                        })
                    }}
                    value={this.state.password}
                    ></TextInput>
                </View>
                <Text style={{textAlign:'center',color:'red'}}>{this.state.failedLogin?this.state.pesanError:""}</Text>
                <TouchableOpacity style={style.button} onPress={()=>this.login()}>
                    <Text style={{ padding: 10, color: 'white', fontSize: 20 }}> Sign In</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const style = StyleSheet.create({
    button: {
        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        marginHorizontal: 40,
        marginVertical: 20,
        backgroundColor: '#34772d'
    },
    inputStyle: {
        width: '70%',
        marginVertical: 20,
        marginHorizontal: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        flexDirection: 'row'
    },
    imageLogo: {
        height: 100,
        width: 130,
        marginTop: 40
    },
    logoHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: '#deffd2'
    }
})
const mapState=(state)=>{
    return{
        isLogin:state.isLogin
    }
}
const functionality=()=>{
    return{
        toLogin:()=>({type:'login'})
    }
}
export default connect(mapState,functionality())(LoginScreen)