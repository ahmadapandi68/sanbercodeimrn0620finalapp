import React, { Component } from 'react';
import {
    StyleSheet, Text, View
    , ScrollView
} from 'react-native';

export default class HalamanDetail extends Component {
    constructor(props){
        super(props)
        
    }
    render() {
        return (
            <ScrollView style={style.container}>
                <View >
                    <View style={style.itemLayout}>
                    <Text style={style.judul}>Akumulasi Data Di Indonesia</Text>
                    </View>
                    <View style={style.itemLayout}>
                        <Text style={style.item}>Indonesia</Text>
                        <Text style={style.item}>Total Positif:  74,018</Text>
                        <Text style={style.item}>Total Sembuh: 34,719</Text>
                        <Text style={style.item}>Total Meninggal:  3,535</Text>
                    </View>
                </View>
            </ScrollView>
            
        )
    }
}
const style = StyleSheet.create({
    item:{
        fontSize:18,
        color:'#4B707F',
        textAlign:"justify"
    },
    judul:{
        fontSize:18,
        color:'#4B707F',
        textAlign:"center"
    },
    itemLayout:{
        borderRadius:5,
        backgroundColor:'#84c968',
        marginStart:20,
        marginEnd:20,
        marginTop:10,
        marginBottom:20,
        padding:10,
        justifyContent:"center",
        alignContent:"center"
    },
    detailAbout:{
        flexDirection:"row",
        height:75,
        marginTop:10
    },
    aboutContainer:{
        height:326,
        width:318,
        backgroundColor:'#84c968',
        margin:20,
        borderRadius:5,
        padding:20

    },
    roundImage:{
        height:151,
        width:146,
        borderRadius:100,
        backgroundColor:'#84c968',
        justifyContent: "center",
        marginStart:100,
        marginTop:50
    },textLayout:{
        height:52,
        width:255,
        borderRadius:7,
        backgroundColor:'#84c968',
        justifyContent: "space-around",
        marginStart:50,
        marginTop:15
    },textName:{
        color:'#4B707F',
        alignContent:"space-around",
        justifyContent:"space-around",
        textAlign:"center",
        fontSize:20,
    },
    button: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        marginHorizontal: 40,
        marginVertical: 20,
        backgroundColor: '#2B0AF3'
    },
    inputStyle: {
        width: '70%',
        marginVertical: 20,
        marginHorizontal: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        flexDirection: 'row'
    },
    container: {
        flex: 1,
        backgroundColor: '#deffd2'
    }
})